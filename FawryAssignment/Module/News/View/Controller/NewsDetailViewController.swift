//
//  NewsDetailViewController.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import UIKit

class NewsDetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newsDescriptionLabel: UILabel!
    @IBOutlet weak var authorDetailLabel: UILabel!
    @IBOutlet weak var sourceDetailLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var newsBannerImageView: UIImageView!
    
    var newsObj: Articles?
    
    // MARK: - UIViewControllerLifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialMethod()
    }
    
    // MARK: - Helper methods
    func initialMethod() {
        self.title = "News Detail"
        if let imageUrl = newsObj?.urlToImage {
            self.newsBannerImageView.kf.setImage(with: URL(string: imageUrl), placeholder: UIImage(named: "newsPlaceholder"))
        } else {
            self.newsBannerImageView.image = UIImage(named: "newsPlaceholder")
        }
        self.newsDescriptionLabel.text = newsObj?.description?.html2String
        self.sourceDetailLabel.text =  newsObj?.source.name
        self.authorDetailLabel.text = newsObj?.author
        self.titleLabel.text = newsObj?.title
    }
    
    @IBAction func navigateButtonAction(_ sender: UIButton) {
        guard let url = URL(string: newsObj?.url ?? "") else { return }
        UIApplication.shared.open(url)
    }
}
