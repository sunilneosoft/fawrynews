//
//  NewsListViewController.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import UIKit

class NewsListViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    lazy var viewModel: NewsViewModel = {
        let viewModel = NewsViewModel(controller: self)
        return viewModel
    }()
    
    // MARK: - UIViewControllerLifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialMethod()
    }
    
    // MARK: - Helper Methods
    func initialMethod() {
        self.navigationController?.navigationBar.isHidden = false
        self.title = "News"
        viewModel.initialize(tableView: tableView, searchBar: searchBar)
        viewModel.readDataFromLocal()
    }
}
