//
//  NewsListTableViewCell.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import UIKit
import Kingfisher

class NewsListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var newsDescriptionLabel: UILabel!
    @IBOutlet weak var newsBannerImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: Set tableViewCell data
    func setData(newsObj: Articles) {
        self.newsDescriptionLabel.text = newsObj.title?.html2String
        if let imageUrl = newsObj.urlToImage {
            self.newsBannerImageView.kf.setImage(with: URL(string: imageUrl), placeholder: UIImage(named: "newsPlaceholder"))
        } else {
            self.newsBannerImageView.image = UIImage(named: "newsPlaceholder")
        }
    }
}
