//
//  NewsViewModel.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import Foundation
import UIKit
import Combine
import NVActivityIndicatorView
import RealmSwift

class NewsViewModel: NSObject {
    
    private var controller: UIViewController?
    var tableView: UITableView?
    var searchBar: UISearchBar?
    var token: AnyCancellable?
    var newsListArray = [Articles]()
    var activityIndicatorView: NVActivityIndicatorView?
    
    // MARK: - Initializer methods
    required init(controller: UIViewController? = nil) {
        self.controller = controller
    }
    
    override private init() {
        
        super.init()
    }
    
    func initialize(tableView: UITableView, searchBar: UISearchBar) {
        self.searchBar = searchBar
        self.searchBar?.delegate = self
        self.tableView = tableView
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.tableView?.estimatedRowHeight = 90
        self.tableView?.rowHeight = UITableView.automaticDimension
        
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let doneBarButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.dismissKeyboard))
        let flexBarButton = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        keyboardToolbar.setItems([flexBarButton, doneBarButton], animated: true)
        searchBar.inputAccessoryView = keyboardToolbar
    }
    
    @objc func dismissKeyboard() {
        controller?.view.endEditing(true)
        if searchBar?.text?.isEmpty ?? false {
            self.callApiForGetNewsList { status in
                if status {
                    self.tableView?.reloadData()
                }
            }
        }
    }
    
}

// MARK: - UITableViewDataSource Delegate methods
extension NewsViewModel: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  let cell = tableView.dequeueReusableCell(withIdentifier: "NewsListTableViewCell")as? NewsListTableViewCell {
            let newsObj = newsListArray[indexPath.row]
            cell.setData(newsObj: newsObj)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // push to required controller
        let vc = NewsDetailViewController.instantiate(fromAppStoryboard: .news)
        vc.newsObj = newsListArray[indexPath.row]
        controller?.push(vc)
    }
}

// MARK: - SearchBar Delegagte methods
extension NewsViewModel: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        controller?.view.endEditing(true)
        if !(searchBar.text?.isEmpty ?? false) {
            self.callApiForGetNewsList(isForSearch: true, searchText: searchBar.text ?? "", completion: { status in
                if status {
                    self.tableView?.reloadData()
                }
            })
        } else {
            self.callApiForGetNewsList { status in
                if status {
                    self.tableView?.reloadData()
                }
            }
        }
    }
}
// MARK: - WebService methods
extension NewsViewModel {
    func callApiForGetNewsList(initialCall: Bool = false, isForSearch: Bool = false, searchText: String = "", completion: ((Bool) -> Void)?) {
        controller?.startActivityIndicator()
        let selectedCountry = UserDefaults.standard.value(forKey: "selectedCountry")as? String ?? ""
        let selectedCategory = UserDefaults.standard.value(forKey: "selectedCategory")as? String ?? ""
        let param = ["country": selectedCountry.lowercased(),
                     "category": selectedCategory.lowercased(),
                     "apiKey": ApiPath.shared.apiKey
        ]
        
        var dateString = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-dd-MM"
        dateString = dateFormatter.string(from: Date())
        
        let searchParam = ["q": searchText.lowercased(),
                           "from": dateString,
                           "sortBy": "publishedAt",
                           "apiKey": ApiPath.shared.apiKey
        ]
        token = NetworkHelper.request(url: ApiPath.shared.topHeadlines, method: .get, param: isForSearch ? searchParam : param, NewsListResponseModel.self, controller: controller).sink { [self] response in
            switch response.result {
            case .success(let response):
                controller?.stopActivityIndicator()
                self.newsListArray = response.articles ?? [Articles]()
                
                if !isForSearch && initialCall {
                    UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: "apiExecutedTime")
                    self.storeNewsInLocal(newsArray: newsListArray)
                } else {
                    self.tableView?.reloadData()
                }
                
                if isForSearch {
                    if newsListArray.isEmpty {
                        controller?.noDataLabel(tableView: self.tableView ?? UITableView(), labelText: "Please search with other text.")
                    } else {
                        controller?.noDataLabel(tableView: self.tableView ?? UITableView(), labelText: "")
                    }
                } else {
                    if newsListArray.isEmpty {
                        controller?.noDataLabel(tableView: self.tableView ?? UITableView(), labelText: "There is no news available for this combination of country and category")
                        UserDefaults.standard.set(false, forKey: "isOnboarded")
                    } else {
                        UserDefaults.standard.set(true, forKey: "isOnboarded")
                    }
                }
            case .failure(let error):
                print(error)
                controller?.stopActivityIndicator()
            }
        }
    }
}

// MARK: - store and retrieve data from local DB
extension NewsViewModel {
    func readDataFromLocal() {
        let realm = try! Realm()
        try! realm.write {
            let result = realm.objects(NewsModel.self)
            if result.count == 0 {
                self.callApiForGetNewsList(initialCall: true, completion: nil)
            } else {
                let apiExecutedTime = UserDefaults.standard.value(forKey: "apiExecutedTime")as? Double ?? 0.0
                // checking for 30 mins
                if Date().timeIntervalSince1970 - apiExecutedTime >= 1800 {
                    self.callApiForGetNewsList(completion: nil)
                } else {
                    for newsObj in result {
                        let newsListObj = Articles.init(author: newsObj.newsAuthorName, title: newsObj.newsTitle, description: newsObj.newsDescription, url: newsObj.newsURL, urlToImage: newsObj.newsImageUrl, content: "", source: Source(id: "", name: newsObj.newsSourceName))
                        self.newsListArray.append(newsListObj)
                        self.tableView?.reloadData()
                    }
                }
            }
        }
    }
    func storeNewsInLocal(newsArray: [Articles]) {
        let realm = try! Realm()
        try! realm.write {
            let result = realm.objects(NewsModel.self)
            realm.delete(result)
        }
        
        try! realm.write {
            for obj in newsArray {
                let newsModelObj = NewsModel()
                newsModelObj.setTitle(title: obj.title ?? "")
                newsModelObj.setNewsDescription(newsDescription: obj.description ?? "")
                newsModelObj.setNewsImageUrl(newsImageUrl: obj.urlToImage ?? "")
                newsModelObj.setNewsAuthorName(newsAuthorName: obj.author ?? "")
                newsModelObj.setNewsSourceName(newsSourceName: obj.source.name ?? "")
                newsModelObj.setNewsURL(newsURL: obj.url ?? "")
                
                realm.add(newsModelObj)
            }
        }
        readDataFromLocal()
    }
}
