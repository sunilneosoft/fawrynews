//
//  NewsModel.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import Foundation
import RealmSwift

// store data to local
public class NewsModel: Object{
    
    @objc dynamic var newsTitle : String = ""
    @objc dynamic var newsImageUrl : String = ""
    @objc dynamic var newsSourceName : String = ""
    @objc dynamic var newsAuthorName : String = ""
    @objc dynamic var newsDescription : String = ""
    @objc dynamic var newsURL : String = ""
    
    public func getTitle() -> String {
        return newsTitle
    }
    public func setTitle(title: String) {
        self.newsTitle = title
    }
    
    public func getnewsImageUrl() -> String {
        return newsImageUrl
    }
    public func setNewsImageUrl(newsImageUrl: String) {
        self.newsImageUrl = newsImageUrl
    }
    
    public func getNewsSourceName() -> String {
        return newsSourceName
    }
    public func setNewsSourceName(newsSourceName: String) {
        self.newsSourceName = newsSourceName
    }
    
    public func getNewsAuthorName() -> String {
        return newsAuthorName
    }
    public func setNewsAuthorName(newsAuthorName: String) {
        self.newsAuthorName = newsAuthorName
    }
    
    public func getNewsDescription() -> String {
        return newsDescription
    }
    public func setNewsDescription(newsDescription: String) {
        self.newsDescription = newsDescription
    }
    
    public func getNewsURL() -> String {
        return newsURL
    }
    public func setNewsURL(newsURL: String) {
        self.newsURL = newsURL
    }
    
}
