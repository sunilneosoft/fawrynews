//
//  NewsListResponseModel.swift
//  FawryAssignment
//
//  Created by Neosoft on 28/10/21.
//

import Foundation
import Kingfisher

struct NewsListResponseModel : Codable {
    let status : String?
    let totalResults : Int?
    let articles : [Articles]?
    
    enum CodingKeys: String, CodingKey {
        
        case status
        case totalResults
        case articles
    }
}

struct Articles : Codable {
    var source = Source(id: "", name: "")
    var author : String?
    var title : String?
    var description : String?
    let url : String?
    var urlToImage : String?
    let content : String?
    
    enum CodingKeys: String, CodingKey {
        case source
        case author
        case title
        case description
        case url
        case urlToImage
        case content
    }
    
    init(author: String, title: String, description: String, url: String, urlToImage: String, content: String, source: Source) {
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.urlToImage = urlToImage
        self.content = content
        self.source.name = source.name
        
    }
}
struct Source : Codable {
    let id: String?
    var name: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
