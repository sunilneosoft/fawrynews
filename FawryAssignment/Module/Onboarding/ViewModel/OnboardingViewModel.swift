//
//  OnboardingViewModel.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import Foundation
import UIKit

class OnboardingViewModel {
    private var controller: UIViewController?
    required init(controller: UIViewController? = nil) {
        self.controller = controller
    }
    var categoryArray = ["Business", "Entertainment", "General", "Health", "Science", "Sports", "Technology"]
    var modelObj = OnboardingModel()
    
    
    // MARK: - Action for select category
    func selectFavouriteCategory(completion: ((Bool) -> Void)?) {
        RPicker.selectOption(title: "Select Category", dataArray: categoryArray, selectedIndex: 0) { selectedText, selectedIndex in
            self.modelObj.selectedCategory = selectedText
            completion?(true)
        }
    }
    
    func checkValidation(completion: ((Bool) -> Void)?) {
        if modelObj.selectedCountry.isEmpty {
            controller?.showAlert(msg: "Please select country.")
            completion?(false)
        } else if modelObj.selectedCategory.isEmpty {
            controller?.showAlert(msg: "Please select your favourite category")
            completion?(false)
        } else {
            completion?(true)
        }
    }
}
