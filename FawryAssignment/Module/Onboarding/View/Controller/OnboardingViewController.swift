//
//  OnboardingViewController.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import UIKit
import CountryPickerView

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var selectCategoryView: UIView!
    @IBOutlet weak var selectCountryView: CountryPickerView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var selectedCategoryLabel: UILabel!
    @IBOutlet weak var selectedCountryLabel: UILabel!
    
    lazy var viewModel: OnboardingViewModel = {
        let viewModel = OnboardingViewModel(controller: self)
        return viewModel
    }()
    
    // MARK: - UIViewControllerLifeCycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialMethod()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - Helper methods
    func initialMethod() {
        selectCountryView.delegate = self
        selectCountryView.dataSource = self
        let categoryGesture = UITapGestureRecognizer(target: self, action: #selector(tapCategoryView))
        self.selectCategoryView.addGestureRecognizer(categoryGesture)
        self.selectCountryView.showCountryCodeInView = false
        self.selectCountryView.showPhoneCodeInView = false
        self.selectCountryView.flagImageView.isHidden = true
    }
    
    // MARK: - Selector methods
    @objc func tapCategoryView() {
        viewModel.selectFavouriteCategory { status in
            if status {
                self.selectedCategoryLabel.text = self.viewModel.modelObj.selectedCategory
            }
        }
    }
    
    // MARK: - UIButton Actions
    @IBAction func submitButtonAction(_ sender: UIButton) {
        viewModel.checkValidation { status in
            if status {
                // pass to next controller
                let vc = NewsListViewController.instantiate(fromAppStoryboard: .news)
                UserDefaults.standard.set(self.viewModel.modelObj.selectedCoutnryCode, forKey: "selectedCountry")
                UserDefaults.standard.set(self.viewModel.modelObj.selectedCategory, forKey: "selectedCategory")
                self.push(vc)
            }
        }
    }
}

extension OnboardingViewController: CountryPickerViewDelegate, CountryPickerViewDataSource {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.selectedCountryLabel.text = country.name
        self.viewModel.modelObj.selectedCountry = country.name
        self.viewModel.modelObj.selectedCoutnryCode = country.code
    }
    
}
