//
//  ApiConstants.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import Foundation
var baseURL = "https://newsapi.org/v2/"

class ApiPath {
    
    static let shared = ApiPath()
    var apiKey = "1a6f62d0d2aa4f03bfe070dd8cceb9f3"
    var topHeadlines = "\(baseURL)\("top-headlines")"
    var everything = "\(baseURL)\("everything")"
}
