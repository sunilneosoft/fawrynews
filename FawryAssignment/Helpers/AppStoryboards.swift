//
//  AppStoryboards.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import Foundation
import UIKit

enum AppStoryboard: String {
case onboarding = "Onboarding"
case news = "News"
}
extension AppStoryboard {
var instance: UIStoryboard {
    return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
}
    func viewController<T: UIViewController>(_ viewControllerClass: T.Type, function: String = #function, line: Int = #line, file: String = #file) -> T {
        
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        return scene
    }

}
