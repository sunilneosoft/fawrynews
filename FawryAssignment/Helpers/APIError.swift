//
//  APIError.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import Foundation
struct APIError: Decodable, Error {
    
    let status: String
    let message: String
    let errorCode: String?
    let errors: [APIError]?
}

@objc protocol ActivityIndicatorDelegate {
    func startActivityIndicator()
    func stopActivityIndicator()
    func checkInternetConnection()
    
}
