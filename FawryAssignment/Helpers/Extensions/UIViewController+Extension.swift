//
//  UIViewController+Extension.swift
//  FawryAssignment
//
//  Created by Neosoft on 27/10/21.
//

import Foundation
import UIKit

extension UIViewController {
    var activityIndicatorHoldingViewTag: Int { return 999999 }

    func showAlert(title: String = "", msg: String, _ completion : (() -> Void)? = nil) {
        let alertViewController = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Okay", style: UIAlertAction.Style.default) { _ in
            alertViewController.dismiss(animated: true, completion: nil)
            completion?()
        }
        alertViewController.addAction(okAction)
        self.present(alertViewController, animated: true, completion: nil)
    }
    func push(_ controller: UIViewController) {
        self.navigationController?.pushViewController(controller, animated: true)
    }
    class var storyboardID: String {
        return "\(self)"
    }
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(self)
    }
    //for show no data label
    func noDataLabel(tableView: UITableView , labelText:String) {
        
        let noDataLabel = UILabel(frame: CGRect(x: 8, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        
        noDataLabel.text    = labelText
        noDataLabel.numberOfLines = 2
        noDataLabel.textColor = UIColor.darkGray
        noDataLabel.textAlignment    = .center
        tableView.backgroundView = noDataLabel
    }
    
    func checkInternetConnection() {
        if !(Reachability.isConnectedToNetwork()) {
            self.showAlert(title: "You are offline now.", msg: "Please turn on Mobile Data or WiFi")
            DispatchQueue.global(qos: .userInitiated).async {
                DispatchQueue.main.async {
                    return
                }
            }
        } else {
            self.startActivityIndicator()
        }
    }
    
    func startActivityIndicator(){
        DispatchQueue.main.async{
            //create holding view
            let holdingView = UIView(frame: UIScreen.main.bounds)
            holdingView.backgroundColor = .white
            holdingView.alpha = 0.3
            
            //Add the tag so we can find the view in order to remove it later
            holdingView.tag = self.activityIndicatorHoldingViewTag
            
            //create activity indicator
            let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.color = .black
            
            //Start animating and add the view
            activityIndicator.startAnimating()
            holdingView.addSubview(activityIndicator)
            self.view.addSubview(holdingView)
        }
    }
    
    func stopActivityIndicator() {
        DispatchQueue.main.async{
            //Here we find the `UIActivityIndicatorView` and remove it from the view
            if let holdingView = self.view.subviews.filter({ $0.tag == self.activityIndicatorHoldingViewTag}).first {
                holdingView.removeFromSuperview()
            }
        }
}
}

// extension for convert html text to normal text
extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}
