//
//  UINavigationBar+Extension.swift
//  FawryAssignment
//
//  Created by Neosoft on 28/10/21.
//
import UIKit

extension UINavigationBar {

    func setAppearance(appearance: UINavigationBarAppearance) {
        standardAppearance = appearance
        compactAppearance = appearance
        scrollEdgeAppearance = appearance
    }
    
    func setDefaultAppearance() {
        // Navigation bar appearance customisation
        let appearance = UINavigationBarAppearance()
        appearance.configureWithDefaultBackground()
        appearance.backgroundColor = .white
        UINavigationBar.appearance().tintColor = .black
        setAppearance(appearance: appearance)
    }
}
